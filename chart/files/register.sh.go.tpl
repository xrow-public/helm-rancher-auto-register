#!/bin/bash

set -x

if [[ -z "$API_TOKEN" ]]; then 
    API_TOKEN=$(curl -sf -k "https://$RANCHER_URI/v3-public/localProviders/local?action=login" -H 'content-type: application/json' --data-binary '{"username": "'$USERNAME'", "password": "'$PASSWORD'", "ttl": 600}' | jq -r .token)
fi

echo "Create the cluster, or get the info on an existing cluster"
CLUSTER_ID=$(curl -s -k "https://$RANCHER_URI/v3/cluster?name=$CLUSTER_NAME" -H 'content-type: application/json' -H "Authorization: Bearer $API_TOKEN" | jq ".data | .[]" | jq -r .id)
if [[ -z "$CLUSTER_ID" ]]; then 
    CLUSTER_ID=$(curl -sf -k "https://$RANCHER_URI/v3/cluster" -H 'content-type: application/json' -H "Authorization: Bearer $API_TOKEN" --data-binary '{"type": "cluster", "name": "'$CLUSTER_NAME'", "enableClusterAlerting":false, "enableClusterMonitoring":false}' | jq -r .id )
    echo "Cluster $CLUSTER_NAME / $CLUSTER_ID created"
else
    echo "Cluster $CLUSTER_NAME / $CLUSTER_ID exists"
    sleep infinity
fi

while true
do
    sleep 5
    echo "Generate the cluster registration token"
    CLUSTER_TOKEN=$(curl -s -k "https://$RANCHER_URI/v3/clusterregistrationtoken" \
        -H 'content-type: application/json' -H "Authorization: Bearer $API_TOKEN" \
        --data-binary '{"type":"clusterRegistrationToken","clusterId":"'$CLUSTER_ID'"}' | jq -r .token )

    if [[ "${CLUSTER_TOKEN}" != "null" && "${CLUSTER_TOKEN}" != "" ]]; then
        break
    fi;
done

URL="https://$RANCHER_URI/v3/import/${CLUSTER_TOKEN}_${CLUSTER_ID}.yaml"
echo "Retrieve and Apply Manifests from $URL"
curl -k "$URL" | kubectl apply -f -

sleep infinity